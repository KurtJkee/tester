﻿namespace WindowsFormsApp2
{
    partial class QuestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TypeCb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ChoiseGb = new System.Windows.Forms.GroupBox();
            this.EnterGb = new System.Windows.Forms.GroupBox();
            this.QuestTb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ListTb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.RightNumTb = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.RightStrTb = new System.Windows.Forms.TextBox();
            this.SaveBt = new System.Windows.Forms.Button();
            this.ChoiseGb.SuspendLayout();
            this.EnterGb.SuspendLayout();
            this.SuspendLayout();
            // 
            // TypeCb
            // 
            this.TypeCb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypeCb.FormattingEnabled = true;
            this.TypeCb.Items.AddRange(new object[] {
            "Выбор правильного ответа",
            "Ввод ответа в пустое поле"});
            this.TypeCb.Location = new System.Drawing.Point(12, 26);
            this.TypeCb.Name = "TypeCb";
            this.TypeCb.Size = new System.Drawing.Size(229, 21);
            this.TypeCb.TabIndex = 11;
            this.TypeCb.SelectedIndexChanged += new System.EventHandler(this.TypeCb_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Тип вопроса";
            // 
            // ChoiseGb
            // 
            this.ChoiseGb.Controls.Add(this.RightNumTb);
            this.ChoiseGb.Controls.Add(this.label4);
            this.ChoiseGb.Controls.Add(this.label3);
            this.ChoiseGb.Controls.Add(this.ListTb);
            this.ChoiseGb.Location = new System.Drawing.Point(12, 133);
            this.ChoiseGb.Name = "ChoiseGb";
            this.ChoiseGb.Size = new System.Drawing.Size(439, 216);
            this.ChoiseGb.TabIndex = 13;
            this.ChoiseGb.TabStop = false;
            this.ChoiseGb.Text = "Выбор ответа";
            // 
            // EnterGb
            // 
            this.EnterGb.Controls.Add(this.RightStrTb);
            this.EnterGb.Controls.Add(this.label6);
            this.EnterGb.Location = new System.Drawing.Point(12, 355);
            this.EnterGb.Name = "EnterGb";
            this.EnterGb.Size = new System.Drawing.Size(439, 81);
            this.EnterGb.TabIndex = 14;
            this.EnterGb.TabStop = false;
            this.EnterGb.Text = "Ввод ответа в текстовое поле";
            // 
            // QuestTb
            // 
            this.QuestTb.Location = new System.Drawing.Point(12, 78);
            this.QuestTb.Multiline = true;
            this.QuestTb.Name = "QuestTb";
            this.QuestTb.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.QuestTb.Size = new System.Drawing.Size(439, 38);
            this.QuestTb.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Вопрос";
            // 
            // ListTb
            // 
            this.ListTb.Location = new System.Drawing.Point(6, 40);
            this.ListTb.Multiline = true;
            this.ListTb.Name = "ListTb";
            this.ListTb.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.ListTb.Size = new System.Drawing.Size(427, 132);
            this.ListTb.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(245, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Список ответов (новый ответ на новой строке)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Номер правильного ответа:";
            // 
            // RightNumTb
            // 
            this.RightNumTb.Location = new System.Drawing.Point(162, 178);
            this.RightNumTb.Name = "RightNumTb";
            this.RightNumTb.Size = new System.Drawing.Size(53, 20);
            this.RightNumTb.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Правильный ответ:";
            // 
            // RightStrTb
            // 
            this.RightStrTb.Location = new System.Drawing.Point(6, 42);
            this.RightStrTb.Name = "RightStrTb";
            this.RightStrTb.Size = new System.Drawing.Size(421, 20);
            this.RightStrTb.TabIndex = 23;
            // 
            // SaveBt
            // 
            this.SaveBt.Location = new System.Drawing.Point(12, 442);
            this.SaveBt.Name = "SaveBt";
            this.SaveBt.Size = new System.Drawing.Size(439, 39);
            this.SaveBt.TabIndex = 17;
            this.SaveBt.Text = "Сохранить вопрос";
            this.SaveBt.UseVisualStyleBackColor = true;
            this.SaveBt.Click += new System.EventHandler(this.SaveBt_Click);
            // 
            // QuestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 495);
            this.Controls.Add(this.SaveBt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.QuestTb);
            this.Controls.Add(this.EnterGb);
            this.Controls.Add(this.ChoiseGb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TypeCb);
            this.Name = "QuestForm";
            this.Text = "Редактирование вопроса";
            this.Load += new System.EventHandler(this.QuestForm_Load);
            this.ChoiseGb.ResumeLayout(false);
            this.ChoiseGb.PerformLayout();
            this.EnterGb.ResumeLayout(false);
            this.EnterGb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox TypeCb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox ChoiseGb;
        private System.Windows.Forms.TextBox RightNumTb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ListTb;
        private System.Windows.Forms.GroupBox EnterGb;
        private System.Windows.Forms.TextBox RightStrTb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox QuestTb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SaveBt;
    }
}