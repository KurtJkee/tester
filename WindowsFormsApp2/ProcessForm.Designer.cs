﻿namespace WindowsFormsApp2
{
    partial class ProcessForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PrevBt = new System.Windows.Forms.Button();
            this.NextBt = new System.Windows.Forms.Button();
            this.SaveBt = new System.Windows.Forms.Button();
            this.questTb = new System.Windows.Forms.TextBox();
            this.questLabel = new System.Windows.Forms.Label();
            this.typelbl = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Timelbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PrevBt
            // 
            this.PrevBt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PrevBt.Location = new System.Drawing.Point(12, 394);
            this.PrevBt.Name = "PrevBt";
            this.PrevBt.Size = new System.Drawing.Size(152, 23);
            this.PrevBt.TabIndex = 0;
            this.PrevBt.Text = "Предыдущий вопрос";
            this.PrevBt.UseVisualStyleBackColor = true;
            this.PrevBt.Click += new System.EventHandler(this.PrevBt_Click);
            // 
            // NextBt
            // 
            this.NextBt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NextBt.Location = new System.Drawing.Point(393, 394);
            this.NextBt.Name = "NextBt";
            this.NextBt.Size = new System.Drawing.Size(152, 23);
            this.NextBt.TabIndex = 1;
            this.NextBt.Text = "Следующий вопрос";
            this.NextBt.UseVisualStyleBackColor = true;
            this.NextBt.Click += new System.EventHandler(this.NextBt_Click);
            // 
            // SaveBt
            // 
            this.SaveBt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SaveBt.Location = new System.Drawing.Point(201, 394);
            this.SaveBt.Name = "SaveBt";
            this.SaveBt.Size = new System.Drawing.Size(152, 23);
            this.SaveBt.TabIndex = 2;
            this.SaveBt.Text = "Завершить тест";
            this.SaveBt.UseVisualStyleBackColor = true;
            this.SaveBt.Click += new System.EventHandler(this.SaveBt_Click);
            // 
            // questTb
            // 
            this.questTb.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.questTb.Location = new System.Drawing.Point(12, 36);
            this.questTb.Multiline = true;
            this.questTb.Name = "questTb";
            this.questTb.ReadOnly = true;
            this.questTb.Size = new System.Drawing.Size(532, 87);
            this.questTb.TabIndex = 3;
            // 
            // questLabel
            // 
            this.questLabel.AutoSize = true;
            this.questLabel.Location = new System.Drawing.Point(13, 13);
            this.questLabel.Name = "questLabel";
            this.questLabel.Size = new System.Drawing.Size(54, 13);
            this.questLabel.TabIndex = 4;
            this.questLabel.Text = "Вопрос #";
            // 
            // typelbl
            // 
            this.typelbl.AutoSize = true;
            this.typelbl.Location = new System.Drawing.Point(13, 130);
            this.typelbl.Name = "typelbl";
            this.typelbl.Size = new System.Drawing.Size(11, 13);
            this.typelbl.TabIndex = 5;
            this.typelbl.Text = "*";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Timelbl
            // 
            this.Timelbl.AutoSize = true;
            this.Timelbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Timelbl.Location = new System.Drawing.Point(187, 359);
            this.Timelbl.Name = "Timelbl";
            this.Timelbl.Size = new System.Drawing.Size(136, 20);
            this.Timelbl.TabIndex = 6;
            this.Timelbl.Text = "До окончания: ";
            // 
            // ProcessForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 429);
            this.Controls.Add(this.Timelbl);
            this.Controls.Add(this.typelbl);
            this.Controls.Add(this.questLabel);
            this.Controls.Add(this.questTb);
            this.Controls.Add(this.SaveBt);
            this.Controls.Add(this.NextBt);
            this.Controls.Add(this.PrevBt);
            this.Name = "ProcessForm";
            this.Text = "ProcessForm";
            this.Load += new System.EventHandler(this.ProcessForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button PrevBt;
        private System.Windows.Forms.Button NextBt;
        private System.Windows.Forms.Button SaveBt;
        private System.Windows.Forms.TextBox questTb;
        private System.Windows.Forms.Label questLabel;
        private System.Windows.Forms.Label typelbl;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label Timelbl;
    }
}