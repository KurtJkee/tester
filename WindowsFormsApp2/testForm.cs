﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class testForm : Form
    {
        bool flag;
        public testForm()
        {
            InitializeComponent();
            flag = false;
        }
        public List<int> ids;
        private void dgr_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            TestChanger t = new TestChanger(ids[dgr.CurrentRow.Index]);
            t.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            t.Show();
        }

        private void testForm_Load(object sender, EventArgs e)
        {
            ids = new List<int>();
            int i = 0;
            foreach (Test t in Singleton.TestDic.Values)
            {
                dgr.Rows.Add();
                dgr[0, i].Value = i + 1;
                dgr[1, i].Value = t.name;
                ++i;
                ids.Add(t.id);
            }
        }

        private void saved(object sender, EventArgs e)
        {
            this.Enabled = true;
            this.Visible = true;
        }

        private void добавитьТестToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newTest t = new newTest(this);
            t.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            t.Show();
        }

        private void удалитьТестToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flag = false;
            Singleton.TestDic.Remove(ids[dgr.CurrentRow.Index]);
            ids.RemoveAt(dgr.CurrentRow.Index);
            dgr.Rows.RemoveAt(dgr.CurrentRow.Index);
        }

        private void сохранитьИзмененияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flag = true;
            Singleton.saveTests();
            MessageBox.Show("Изменения успешно сохранены!", "Оповещение", MessageBoxButtons.OK);
        }

        private void testForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (flag) 
            {
                Application.Exit();
                return;
            }
            else flag = true;
            if (MessageBox.Show("Сохранить изменения(тесты)?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveTests();
            Application.Exit();
        }

        private void сменитьРежимToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flag = true;
            if (MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveTests();
            Form2 f = new Form2();
            f.Show();
            this.Hide();
        }

        private void сменитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EnterForm f = new EnterForm();
            if (MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveTests();
            f.Show();
            flag = true;
            this.Hide();
        }

        private void выходИзПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
