﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace WindowsFormsApp2
{
    public class Quest
    {
        public String question;
        public int type;
        public Quest() { question = ""; }
        public virtual bool check(String ans) { return true; }
        public virtual void read(StreamReader reader) { }
        public virtual void save(StreamWriter writer) { }
        public virtual String getRight() { return ""; }
    }

    public class enumQuest: Quest
    {
        public List<String> variants;
        public int right;
        public enumQuest() { variants = new List<String>(); type = 0; }
        public enumQuest(String _question, List<String> _variants, int _right)
        {
            type = 0;
            question = _question;
            variants = _variants;
            right = _right;
        }
        public override bool check(string ans)
        {
            return right == Convert.ToInt32(ans);
        }
        public override void read(StreamReader reader)
        {
            question = reader.ReadLine();
            int n = Convert.ToInt32(reader.ReadLine());
            right = Convert.ToInt32(reader.ReadLine());
            for (int i = 0; i < n; ++i) variants.Add(reader.ReadLine());
        }
        public override void save(StreamWriter writer)
        {
            writer.WriteLine(type);
            writer.WriteLine(question);
            writer.WriteLine(variants.Count);
            writer.WriteLine(right);
            foreach (String s in variants) writer.WriteLine(s);
        }
        public override string getRight()
        {
            return Convert.ToString(right);
        }
    }

    public class textQuest : Quest
    {
        public String ans;
        public textQuest() { type = 1; }
        public textQuest(String _question, String _ans)
        {
            type = 1;
            question = _question;
            ans = _ans;
        }
        public override bool check(string userans)
        {
            String a="", u="";
            foreach(char c in ans)
            {
                if (c != ' ' && c != '\t' && c != '\n') a += c;
            }
            foreach (char c in userans)
            {
                if (c != ' ' && c != '\t' && c != '\n') u += c;
            }
            return (a.ToLower() == u.ToLower());

        }
        public override void read(StreamReader reader)
        {
            question = reader.ReadLine();
            ans = reader.ReadLine();
        }
        public override void save(StreamWriter writer)
        {
            writer.WriteLine(type);
            writer.WriteLine(question);
            writer.WriteLine(ans);
        }
        public override string getRight()
        {
            return ans;
        }
    }

    public class Test
    {
        public List<Quest> lst;
        public string name;
        public int time, id;
        public Test() {
            lst = new List<Quest>();
            if (Singleton.TestDic.Count > 0) id = Singleton.TestDic.Last().Value.id+1;
            else id = 1;
        }
        public Test(List<Quest> _lst) { lst = _lst; }
        public void read(StreamReader reader)
        {
            name = reader.ReadLine();
            id = Convert.ToInt32(reader.ReadLine());
            time = Convert.ToInt32(reader.ReadLine());
            int type, n = Convert.ToInt32(reader.ReadLine());
            for (int i = 0; i < n; ++i)
            {
                Quest q;
                type = Convert.ToInt32(reader.ReadLine());
                if (type == 0) q = new enumQuest();
                else q = new textQuest();
                q.read(reader);
                lst.Add(q);
            }
        }
        public void save(StreamWriter writer)
        {
            writer.WriteLine(name);
            writer.WriteLine(id);
            writer.WriteLine(time);
            writer.WriteLine(lst.Count);
            foreach (Quest q in lst) q.save(writer);
        }
    }

    public class QuestResult
    {
        public int id;
        public virtual String getValue() { return ""; }
        public virtual void read(StreamReader reader) { }
        public virtual void save(StreamWriter writer) { }
        public virtual bool check(Quest q) { return true; }
        public virtual void setValue(String s) { }
    }
    public class stringRes : QuestResult
    {
        public String ans;
        public stringRes() { ans = ""; }
        public stringRes(int _id, String _ans)
        {
            id = _id;
            ans = _ans;
        }
        public override string getValue()
        {
            return ans;
        }
        public override void read(StreamReader reader)
        {
            ans = reader.ReadLine();
        }
        public override bool check(Quest q)
        {
            return q.check(ans);
        }
        public override void save(StreamWriter writer)
        {
            writer.WriteLine("1");
            writer.WriteLine(ans);
        }
        public override void setValue (String s)
        {
            ans = s;
        }
    }
    public class intRes : QuestResult
    {
        public int ans;
        public intRes() { ans = 0; }
        public intRes(int _id, int _ans)
        {
            id = _id;
            ans = _ans;
        }
        public override string getValue()
        {
            return Convert.ToString(ans);
        }
        public override void read(StreamReader reader)
        {
            ans = Convert.ToInt32(reader.ReadLine());
        }
        public override bool check(Quest q)
        {
            return q.check(Convert.ToString(ans));
        }
        public override void save(StreamWriter writer)
        {
            writer.WriteLine("0");
            writer.WriteLine(ans);
        }
        public override void setValue(String s)
        {
            ans = Convert.ToInt32(s);
        }
    }
    public class TestResult
    {
        public int id, right;
        public List<QuestResult> lst;
        public TestResult()
        {
            lst = new List<QuestResult>();
        }
        public void read(StreamReader reader)
        {
            id = Convert.ToInt32(reader.ReadLine());
            int n = Convert.ToInt32(reader.ReadLine()), type;
            QuestResult tmp;
            for(int i=0; i<n; ++i)
            {
                type = Convert.ToInt32(reader.ReadLine());
                if (type == 0) tmp = new intRes();
                else tmp = new stringRes();
                tmp.read(reader);
                tmp.id = i;
                if (tmp.check(Singleton.TestDic[id].lst[i])) ++right;
                lst.Add(tmp);
            }
        }
        public void save (StreamWriter writer)
        {
            writer.WriteLine(id);
            writer.WriteLine(lst.Count);
            foreach (QuestResult t in lst) t.save(writer);
        }
    }
    public class ListResult
    {
        public List<TestResult> lst;
        public ListResult() { lst = new List<TestResult>(); }
        public void read(StreamReader reader)
        {
            int n = Convert.ToInt32(reader.ReadLine());
            for(int i=0; i<n; ++i)
            {
                TestResult tmp = new TestResult();
                tmp.read(reader);
                lst.Add(tmp);
            }
        }
        public void save(StreamWriter writer)
        {
            writer.WriteLine(lst.Count);
            foreach (TestResult t in lst) t.save(writer);
        }
                
    }

    public class UserInfo
    {
        public String name, pass;
        public int type;
        public ListResult lst;
        public UserInfo() { lst = new ListResult(); }
        public UserInfo(String _name, String _pass, int _type)
        {
            name = _name;
            pass = _pass;
            type = _type;
            lst = new ListResult();
        }
        public void read(StreamReader reader)
        {
            name = reader.ReadLine();
            pass = reader.ReadLine();
            type = Convert.ToInt32(reader.ReadLine());
            if (type != 0) lst.read(reader);
        }
        public void save(StreamWriter writer)
        {
            writer.WriteLine(name);
            writer.WriteLine(pass);
            writer.WriteLine(type);
            if (type != 0) lst.save(writer);
        }
    }
}
