﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace WindowsFormsApp2
{
    class Singleton
    {
        public static Dictionary<String, UserInfo> UserDic;
        public static Dictionary<int, Test> TestDic;
        public static UserInfo cur;
        static Singleton inst;
        public Singleton()
        {
            TestDic = new Dictionary<int, Test>();
            UserDic = new Dictionary<string, UserInfo>(); 
            bool flag = true;
            StreamReader reader=null;
            while (flag)
            {
                try
                {
                    reader = new StreamReader("Tests.txt");
                    flag = false;
                }
                catch { }
            }
            int n = Convert.ToInt32(reader.ReadLine());
            for (int i = 0; i < n; ++i)
            {
                Test t = new Test();
                t.read(reader);
                TestDic.Add(t.id, t);
            }
            reader.Close();
            flag = true;
            while (flag)
            {
                try
                {
                    reader = new StreamReader("Pass.txt");
                    flag = false;
                }
                catch { }
            }
            n = Convert.ToInt32(reader.ReadLine());
            for (int i = 0; i < n; ++i)
            {
                UserInfo u = new UserInfo();
                u.read(reader);
                UserDic.Add(u.name, u);
            }
            reader.Close();
        }
        public static bool changeUser(String name, String pass)
        {
            if (UserDic.ContainsKey(name) && UserDic[name].pass == pass)
            {
                cur = UserDic[name];
                return true;
            }
            return false;
        }
        public static void init()
        {
            if (inst == null) inst = new Singleton();            
        }
        public static void saveUsers()
        {
            StreamWriter writer = new StreamWriter("pass.txt");
            writer.WriteLine(UserDic.Count);
            foreach (UserInfo u in UserDic.Values) u.save(writer);
            writer.Close();
        }
        public static void saveTests()
        {
            StreamWriter writer = new StreamWriter("Tests.txt");
            writer.WriteLine(TestDic.Count);
            foreach (Test t in TestDic.Values) t.save(writer);
            writer.Close();
        }
    }
}
