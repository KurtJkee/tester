﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class newUser : Form
    {
        User uf;
        public newUser(User _uf)
        {
            uf = _uf;
            InitializeComponent();
            TypeCb.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserInfo u = new UserInfo(LoginTb.Text, PassTb.Text, TypeCb.SelectedIndex);
            if(Singleton.UserDic.ContainsKey(u.name))
            {
                MessageBox.Show("Пользователь с таким именем уже существует!", "Ошибка", MessageBoxButtons.OK);
                return;
            }
            Singleton.UserDic.Add(u.name, u);
            uf.dgr.Rows.Add();
            uf.dgr[0, uf.dgr.Rows.Count - 1].Value = uf.dgr.Rows.Count;
            uf.dgr[1, uf.dgr.Rows.Count - 1].Value = u.name;
            this.Close();
        }

        private void newUser_Load(object sender, EventArgs e)
        {

        }
    }
}
