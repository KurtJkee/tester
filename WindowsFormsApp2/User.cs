﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class User : Form
    {
        bool flag;
        Form2 f2;
        public User(Form2 _f2)
        {
            f2  = _f2;
            InitializeComponent();
            flag = false;
        }

        private void добавитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newUser u = new newUser(this);
            u.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            u.Show();
            flag = false;
        }

        private void удалитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.UserDic.Remove(dgr[1, dgr.CurrentRow.Index].Value.ToString());
            dgr.Rows.RemoveAt(dgr.CurrentRow.Index);
            flag = false;
        }

        private void User_Load(object sender, EventArgs e)
        {
            int i = 0;
            foreach(UserInfo u in Singleton.UserDic.Values)
            {
                dgr.Rows.Add();
                dgr[0, i].Value = i + 1;
                dgr[1, i].Value = u.name;
                ++i;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgr_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            userinfo u = new userinfo(dgr[1, dgr.CurrentRow.Index].Value.ToString());
            u.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            u.Show();
            flag = false;
        }
        private void saved(object sender, EventArgs e) {
            this.Enabled = true;
            this.Visible = true;
        }

        private void сохранитьИзмененияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.saveUsers();
            MessageBox.Show("Изменения успешно сохранены!", "Оповещение", MessageBoxButtons.OK);
            flag = true;
        }

        private void User_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void User_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (flag) 
            {
                Application.Exit();
                return;
            }
            flag = true;
            if (MessageBox.Show("Сохранить изменения(пользователи)?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveUsers();
            Application.Exit();
        }

        private void выходИзПрограммыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void сменитьРежимToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveUsers();
            flag = true;
            f2.Show();
            this.Hide();
        }

        private void сменитьПользователяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EnterForm f = new EnterForm();
            if (MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Singleton.saveUsers();
            flag = true;
            f.Show();
            this.Hide();
        }
    }
}
