﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class ResultForm : Form
    {
        TestResult t;
        public ResultForm(TestResult _t)
        {
            InitializeComponent();
            t = _t;
        }

        private void ResultForm_Load(object sender, EventArgs e)
        {
            label1.Text += Singleton.TestDic[t.id].name;
            label2.Text += Singleton.TestDic[t.id].time;
            int i = 0;
            foreach (QuestResult q in t.lst)
            {
                dgr.Rows.Add();
                dgr[0, i].Value = i + 1;
                dgr[1, i].Value = Singleton.TestDic[t.id].lst[i].question;
                dgr[2, i].Value = q.getValue();
                dgr[3, i].Value = Singleton.TestDic[t.id].lst[i].getRight();
                ++i;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dgr_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgr_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }
    }
}
