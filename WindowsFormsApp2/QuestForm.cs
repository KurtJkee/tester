﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class QuestForm : Form
    {
        TestChanger f;
        int testId, questId;
        public QuestForm(TestChanger _f, int _testId, int _questId)
        {
            InitializeComponent();
            f = _f;
            testId = _testId;
            questId = _questId;
        }

        private void TypeCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChoiseGb.Enabled = TypeCb.SelectedIndex == 0;
            EnterGb.Enabled = TypeCb.SelectedIndex == 1;
        }

        private void SaveBt_Click(object sender, EventArgs e)
        {
            Quest q;
            String QuestText="", RightText="";
            foreach (String line in QuestTb.Lines)
            {
                if (line.Length > 0) QuestText+=line;
            }
            foreach (String line in RightStrTb.Lines)
            {
                if (line.Length > 0) RightText += line;
            }
            if (TypeCb.SelectedIndex == 0)
            {
                List<String> lst = new List<String>();
                foreach (String line in ListTb.Lines)
                {
                    if (line.Length > 0) lst.Add(line);
                }
                q = new enumQuest(QuestText, lst, Convert.ToInt32(RightNumTb.Text));
            }
            else q = new textQuest(QuestText, RightText);
            if (questId == Singleton.TestDic[testId].lst.Count)
            {
                Singleton.TestDic[testId].lst.Add(q);
                f.dgr.Rows.Add();
                f.dgr[0, f.dgr.Rows.Count-1].Value = f.dgr.Rows.Count;
                f.dgr[1, f.dgr.Rows.Count-1].Value = q.question;
            }
            else Singleton.TestDic[testId].lst[questId] = q;
            this.Close();
        }

        private void QuestForm_Load(object sender, EventArgs e)
        {
            if (questId < f.dgr.Rows.Count)
            {
                Quest f = Singleton.TestDic[testId].lst[questId];
                TypeCb.SelectedIndex = f.type;
                QuestTb.Text = f.question;
                if (f.type == 0)
                {
                    enumQuest q = (enumQuest)f;
                    RightNumTb.Text = Convert.ToString(q.right);
                    foreach (String str in q.variants) ListTb.Text += str + Environment.NewLine;
                }
                else
                {
                    textQuest t = (textQuest)f;
                    RightStrTb.Text = t.ans;
                }
            }
            else TypeCb.SelectedIndex = 0;
        }
    }
}
