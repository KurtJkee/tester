﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class newTest : Form
    {
        testForm f;
        public newTest(testForm _f)
        {
            f = _f;
            InitializeComponent();
        }

        private void SaveBt_Click(object sender, EventArgs e)
        {
            Test t = new Test();
            t.name = NameTb.Text;
            Singleton.TestDic.Add(t.id, t);
            f.dgr.Rows.Add();
            f.dgr[0, f.dgr.Rows.Count - 1].Value = f.dgr.Rows.Count;
            f.dgr[1, f.dgr.Rows.Count - 1].Value = t.name;
            f.ids.Add(t.id);
            this.Close();
        }

        private void newTest_Load(object sender, EventArgs e)
        {

        }
    }
}
