﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class TestChanger : Form
    {
        int id;
        public TestChanger(int _id)
        {
            id = _id;
            InitializeComponent();
        }

        private void dgr_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TestChanger_Load(object sender, EventArgs e)
        {
            int i = 0;
            TimeTb.Text = Singleton.TestDic[id].time.ToString();
            foreach (Quest t in Singleton.TestDic[id].lst)
            {
                dgr.Rows.Add();
                dgr[0, i].Value = i + 1;
                dgr[1, i].Value = t.question;
                ++i;
            }
        }

        private void saved(object sender, EventArgs e)
        {
            this.Enabled = true;
            this.Visible = true;
        }

        private void добавитьВопросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuestForm q = new QuestForm(this, id, dgr.Rows.Count);
            q.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            q.Show();
        }

        private void dgr_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            QuestForm q = new QuestForm(this, id, dgr.CurrentRow.Index);
            q.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            q.Show();
        }

        private void удалитьВопросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.TestDic[id].lst.RemoveAt(dgr.CurrentRow.Index);
            dgr.Rows.RemoveAt(dgr.CurrentRow.Index);
        }

        private void сохранитьИзмененияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.saveTests();
            MessageBox.Show("Изменения успешно сохранены!", "Оповещение", MessageBoxButtons.OK);
        }

        private void TimeTb_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int i = Convert.ToInt32(TimeTb.Text);
                Singleton.TestDic[id].time = i;
            }
            catch
            {
                MessageBox.Show("Неправильый формат ввода");
            }
        }
    }
}
