﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class userinfo : Form
    {
        UserInfo u;
        bool changed;
        public userinfo(String name)
        {
            changed = false;
            u= Singleton.UserDic[name];
            InitializeComponent();
        }

        private void userinfo_Load(object sender, EventArgs e)
        {
            LoginTb.Text = u.name;
            PassTb.Text = u.pass;
            TypeCb.SelectedIndex = u.type;
            int i = 0;
            foreach (TestResult t in u.lst.lst)
            {
                if (!Singleton.TestDic.ContainsKey(t.id)) continue;
                dgr.Rows.Add();
                dgr[0, i].Value = i + 1;
                dgr[1, i].Value = Singleton.TestDic[t.id].name;
                dgr[2, i].Value = Convert.ToString(t.right) + "/" + Convert.ToString(Singleton.TestDic[t.id].lst.Count);
            }
        }

        private void ResetBt_Click(object sender, EventArgs e)
        {
            if (dgr.Rows.Count == 0) return;
            Singleton.UserDic[u.name].lst.lst.RemoveAt(dgr.CurrentRow.Index);
            dgr.Rows.RemoveAt(dgr.CurrentRow.Index);
            changed = true;
        }

        private void userinfo_FormClosed(object sender, FormClosedEventArgs e)
        {
           
        }

        private void userinfo_FormClosing(object sender, FormClosingEventArgs e)
        {
           if (LoginTb.Text != u.name || PassTb.Text != u.pass || TypeCb.SelectedIndex != u.type || changed)
           {
                if (MessageBox.Show("Сохранить изменения о пользователе?", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    Singleton.UserDic.Remove(u.name);
                    u.name = LoginTb.Text;
                    u.pass = PassTb.Text;
                    u.type = TypeCb.SelectedIndex;
                    if (Singleton.UserDic.ContainsKey(u.name))
                    {
                        MessageBox.Show("Пользователь с таким именем уже существует!", "Ошибка", MessageBoxButtons.OK);
                        e.Cancel = true;
                        return;
                    }
                    Singleton.UserDic.Add(u.name, u);
                }
                Singleton.saveUsers();
           }
        }

        private void saved(object sender, EventArgs e)
        {
            this.Enabled = true;
            this.Visible = true;
        }

        private void dgr_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ResultForm f = new ResultForm(u.lst.lst[dgr.CurrentRow.Index]);
            f.FormClosed += new FormClosedEventHandler(saved);
            this.Enabled = false;
            f.Show();
        }

        private void dgr_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
