﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp2
{
    public partial class EnterForm : Form
    {     
        public EnterForm()
        {
            InitializeComponent();
            Singleton.init();           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Singleton.changeUser(LoginTb.Text, PassTb.Text))
            {
                if(Singleton.cur.type==0)
                {
                    Form2 f = new Form2();
                    f.Show();
                    this.Hide();
                }
                else
                {
                    forUser f = new forUser();
                    f.Show();
                    this.Hide();
                }
            }
            else MessageBox.Show("Данная комбинация логина и пароля не найдена");
        }

        private void EnterForm_Load(object sender, EventArgs e)
        {

        }

        private void EnterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
