﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class forUser : Form
    {
        public forUser()
        {
            InitializeComponent();
        }
        List<int> ids;
        private void forUser_Load(object sender, EventArgs e)
        {
            ids = new List<int>();
            label1.Text += Singleton.cur.name;
            foreach (Test t in Singleton.TestDic.Values) 
            {
                TestCb.Items.Add(t.name);
                ids.Add(t.id);
            }
        }
        private void saved(object sender, EventArgs e)
        {
            this.Enabled = true;
            this.Visible = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            foreach(TestResult t in Singleton.cur.lst.lst)
            {
                if(t.id == ids[TestCb.SelectedIndex])
                {
                    MessageBox.Show("Вы уже проходили это тестирование");
                    return;
                }
            }
            if (MessageBox.Show("Вы уверены, что хотите начать? На тест отводится минут: " + Singleton.TestDic[ids[TestCb.SelectedIndex]].time.ToString() + ". Перепроходить тест нельзя.", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                ProcessForm f = new ProcessForm(ids[TestCb.SelectedIndex]);
                f.FormClosed += new FormClosedEventHandler(saved);
                this.Enabled = false;
                f.Show();
            }
        }

        private void forUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
