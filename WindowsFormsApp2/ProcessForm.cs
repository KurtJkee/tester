﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class ProcessForm : Form
    {
        int id, cur;
        TextBox tb;
        List<RadioButton> rdbtns;
        DateTime start;
        bool flag = false;
        public ProcessForm(int _id)
        {
            InitializeComponent();
            id = _id;
            cur = 0;
            this.Text = Singleton.TestDic[id].name;
        }
        private void save()
        {
            String ans="0";
            if (Singleton.TestDic[id].lst[cur].type == 0)
            {
                int i = 1;
                foreach (RadioButton r in rdbtns)
                {
                    if (r.Checked)
                    {
                        ans = i.ToString();
                        break;
                    }
                    ++i;
                }
            }
            else ans = tb.Text;
            Singleton.cur.lst.lst.Last().lst[cur].setValue(ans);
        }
        private void makeQ()
        {
            foreach (RadioButton r in rdbtns)
            {
                if (Controls.Contains(r)) Controls.Remove(r);
                r.Checked = false;
            }
            if(Controls.Contains(tb)) Controls.Remove(tb);
            questLabel.Text = "Вопрос #" + (cur+1).ToString();
            questTb.Text = Singleton.TestDic[id].lst[cur].question;
            if (Singleton.TestDic[id].lst[cur].type == 0)
            {
                typelbl.Text = "Выберите ответ из предложенных вариантов";
                enumQuest q = (enumQuest)Singleton.TestDic[id].lst[cur];
                int i = 0;
                foreach (String s in q.variants)
                {
                    rdbtns[i].Text = s;
                    Controls.Add(rdbtns[i]);
                  
                    ++i;
                }
                if(Convert.ToInt32(Singleton.cur.lst.lst.Last().lst[cur].getValue())>0) rdbtns[Convert.ToInt32(Singleton.cur.lst.lst.Last().lst[cur].getValue())-1].Checked = true;
            }
            else 
            {
                typelbl.Text = "Введите ответ в пустое поле";
                tb.Text = Singleton.cur.lst.lst.Last().lst[cur].getValue();
                Controls.Add(tb);
            }
        }
        private void PrevBt_Click(object sender, EventArgs e)
        {
            save();
            --cur;
            if (cur < 0) cur = 0;
            else makeQ();
        }

        private void NextBt_Click(object sender, EventArgs e)
        {
            save();
            ++cur;
            if (cur == Singleton.TestDic[id].lst.Count) --cur;
            else makeQ();
        }
        String converttime(int sec)
        {
            sec = Singleton.TestDic[id].time * 60 - sec;
            return (sec / 60).ToString() + ":" + (sec%60<10?"0":"") + (sec % 60);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            Timelbl.Text = "До окончания: " + converttime((int)(DateTime.Now - start).TotalSeconds).ToString();
            if((int)(DateTime.Now - start).TotalSeconds > Singleton.TestDic[id].time*60)
            {
                this.Close();
                MessageBox.Show("Время вышло");
                Singleton.saveUsers();
                flag = true;

            }
        }

        private void SaveBt_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Вы уверены, что хотите закончить тестирование? Тест нельзя будет пройти заново.", "Подтверждение", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                flag = true;
                Singleton.saveUsers();
                this.Close();
            }
        }

        private void ProcessForm_Load(object sender, EventArgs e)
        {
            Timelbl.Text = "До окончания: " + converttime(0);
            TestResult t = new TestResult();
            t.id = id;
            int i = 0;
            foreach (Quest q in Singleton.TestDic[id].lst)
            {
                if (q.type == 0) t.lst.Add(new intRes());
                else t.lst.Add(new stringRes());
                t.lst.Last().id = i++;
            }
            Singleton.cur.lst.lst.Add(t);
            rdbtns = new List<RadioButton>();
            for (int k = 0; k < 10; ++k)
            {
                RadioButton r = new RadioButton();
                r.AutoSize = true;
                r.Location = new Point(20, 150+k*30);
                rdbtns.Add(r);
            }
            tb = new TextBox();
            tb.Location = new Point(20, 150);
            makeQ();
            start = DateTime.Now;
            timer1.Start();
        }
    }
}
